#include "main.h"

using namespace std;
using namespace cv;

int main(int argc, char *argv[]) {
  QCoreApplication a(argc, argv);

  VideoCapture cap(0);

  // load cascades
  if (!face_cascade.load(face_cascade_name)) {
    cout << "Error loading face detection file!"
         << "\n";
  }

  if (!eyes_cascade.load(eyes_cascade_name)) {
    cout << "Error loading eye detection file!"
         << "\n";
  }

  if (!cap.isOpened()) {
    cout << "Can't open camera!"
         << "\n";
    return -1;
  }

  double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH);
  double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT);

  cout << "Frame size: " << dWidth << "x" << dHeight << endl;

  while (true) {
    Mat frame;
    cap.read(frame);
    if (frame.empty()) {
      cout << "Can't read from camera!" << endl;
      return -1;
    }
    imshow("Live footage", frame);
    if (waitKey(1) == 30) { // esc key
      cout << "esc pressed!" << endl;
      break;
    }
    DetectAndDisplay(frame);
  }
  return a.exec();
}

void DetectAndDisplay(Mat frame) {
  vector<Rect> faces, eyes;
  Mat frame_gray, faceROI;

  cvtColor(frame, frame_gray, CV_BGR2GRAY);

  //    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2,
  //                                  0|CV_HAAR_SCALE_IMAGE, Size(30,30));
  face_cascade.detectMultiScale(frame_gray, faces, 1.3, 5);

  for (size_t i = 0; i < faces.size(); i++) {
    Rect centerFace(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
    rectangle(frame, centerFace, Scalar(255, 0, 255), 4, 8, 0);

    faceROI = frame_gray(faces[i]);

    //-- In each face, detect eyes
    //        eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2,
    //                                       0 |CV_HAAR_SCALE_IMAGE, Size(30,
    //                                       30));
    eyes_cascade.detectMultiScale(faceROI, eyes);
    for (size_t j = 0; j < eyes.size(); j++) {
      Rect centerEye(faces[i].x + eyes[j].x, faces[i].y + eyes[j].y,
                     eyes[j].width, eyes[j].height);
      rectangle(frame, centerEye, Scalar(255, 0, 0), 1, 8, 0);
    }
  }
  imshow(window_name, frame);
}
