#ifndef MAIN_H
#define MAIN_H

#include <QCoreApplication>
#include <vector>
#include <string>
#include <iostream>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

void DetectAndDisplay(Mat frame);

String face_cascade_name = "haarcascade_frontalface_default.xml";
String eyes_cascade_name = "haarcascade_eye.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
string window_name = "Face Detection";
RNG rng(12345);

#endif // MAIN_H
